package com.example.summative4.pojo;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotBlank(message = "Not Blank")
    private String name;
    @NotBlank(message = "Not Blank")
    private String lastName;
    @NotBlank(message = "Not Blank")
    private String email;
    @NotBlank(message = "Not Blank")
    private String birthDate;
    @ManyToOne(targetEntity = Department.class, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "department_id")
    private Department departmentId;

    public User(){

    }

    public User(int id, String name, String lastName, String email, String birthDate, Department departmentId) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.departmentId = departmentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Department getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Department departmentId) {
        this.departmentId = departmentId;
    }
}
