package com.example.summative4.repository;

import com.example.summative4.pojo.Department;
import com.example.summative4.pojo.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DepartmentRepo extends CrudRepository<Department, Integer> {
    Department findById(int id);
    List<Department> findAll();
    void deleteById(Department id);
    Department save(Department department);
}
