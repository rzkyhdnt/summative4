package com.example.summative4.repository;

import com.example.summative4.pojo.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Integer> {
    User findById(int id);
    User findByName(String name);
    List<User> findAll();
    void deleteById(User id);
    User save(User user);
}
