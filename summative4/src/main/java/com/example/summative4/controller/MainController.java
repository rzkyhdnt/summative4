package com.example.summative4.controller;

import com.example.summative4.pojo.Department;
import com.example.summative4.pojo.User;
import com.example.summative4.repository.DepartmentRepo;
import com.example.summative4.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    UserRepo userRepo;

    @Autowired
    DepartmentRepo departmentRepo;


    @ModelAttribute
    public void addDepartment(Model model){
        List<Department> departmentList = new ArrayList<Department>(departmentRepo.findAll());
        model.addAttribute("departments", departmentList);
    }

    @ModelAttribute
    public void addUser(Model model){
        List<User> userList = new ArrayList<>(userRepo.findAll());
        model.addAttribute("users", userList);
    }

    @GetMapping("/")
    public ModelAndView show(Model model){
        User user = new User();
        model.addAttribute("user", user);
        ModelAndView mav = new ModelAndView("form");
        return mav;
    }

    @GetMapping("/{id}")
    public String deleted(@PathVariable("id") int id, Model model){
        User user = userRepo.findById(id);
        userRepo.delete(user);
        return "redirect:/";
    }

    @PostMapping(value = "/")
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return"redirect:/";
        } else {
            userRepo.save(user);
            return "redirect:/";
        }
    }

    @PostMapping(value = "/json")
    public String addJson(@Valid @ModelAttribute("user") ArrayList<User> user, BindingResult bindingResult){
        ArrayList<User> temp = new ArrayList<>();
        if(bindingResult.hasErrors()){
            return "redirect:/";
        } else {
            for(User usr : user){
                temp.add(usr);
            }
            System.out.println(temp);
            return "redirect:/";
        }
    }

    @DeleteMapping(value = "/{id}")
    public String delete(@PathVariable("id") int id, Model model){
        User user = userRepo.findById(id);
        userRepo.delete(user);
        return "redirect:/";
    }
}
