$(document).ready(function(){
    $('.table .delBtn').on('click', function(event){
        event.preventDefault();
        var href = $(this).attr('href');
        $('#myModal #delRef').attr('href', href);
        $('#myModal').modal();
    });
});

function onSubmit(){
    var formData = new FormData();
    formData.append("user", new Blob([JSON.stringify({
        "name" : document.getElementById("name").value,
        "last_name" : document.getElementById("lastname").value,
        "email" : document.getElementById("email").value,
        "birth_date" : document.getElementById("birthdate").value,
        "department_id" : document.getElementById("department").value
    })], {
        type: "application/json"
    }));

    fetch(formData).then(function (response){
        return response.json();
    }).then(data => {
        let post = {
            method : "POST",
            headers : {
                'Content-type' : 'application/json'
            },
            body : formData
        }
        let send = fetch("http://localhost:8080/", post)
    }).catch(function (err) {
        console.warn("Something went wrong!", err);
    })
}

function alertSign(){
    if(document.getElementById("name").value === null){
        alert("Data ada yg kosong!");
    } else {
        alert("Data berhasil disimpan!");
    }
}
